// Herní proměnné
let score = 0;
let gameActive = false;
let enemyCount = 1;
let enemySpeed = 0.2;
let enemyHealth = 3;
let shotSpeed = 20;
let enemySpawnInterval = 5000;
let enemySpawnIntervalMin = 4000;
let enemySpawnIntervalMax = 7000;
// Příznak pro sledování prvního kliknutí na tlačítko "Mute"
let firstMuteClick = true;
let enemiesPerSpawn = 1; // Počáteční počet nepřátel na jedno objevení
// Globální proměnná pro uložení hlasitosti
let volume = 0;

// Audio pro hudbu
let menuMusic = playSound('music/MainMenu.mp3', true);
let gameMusic;

// Příznak pro spuštění prvního nepřítele
let firstEnemyLaunched = false;

// Vytvoříme element pro zobrazení skóre
let scoreElement = document.getElementById('score');
scoreElement.style.position = 'absolute';
scoreElement.style.top = '14px';
scoreElement.style.left = '14px';
scoreElement.style.color = 'black';
document.body.appendChild(scoreElement);

// --- Prototypy a jmenné prostory ---

// Jmenný prostor pro nepřátele
const EnemyNamespace = {
    // Prototyp nepřítele
    EnemyPrototype: {
        width: 35,
        height: 35,
        health: enemyHealth,
        speed: enemySpeed,
        dx: 0,
        dy: 0,
        isDamaged: false,
        isDodging: false,
        dodgeDirection: 1,
        dodgeTimer: 0,

        // Metoda pro aktualizaci stavu nepřítele
        update: function() {
            if (score >= 7 && !this.isDodging) {
                this.isDodging = true;
                this.dodgeDirection = Math.random() < 0.5 ? -1 : 1;
                this.dodgeTimer = Math.floor(Math.random() * 4000) + 8000;
            }

            if (this.isDodging) {
                this.x -= this.speed * 0.8;
                this.y += this.dodgeDirection * this.speed;

                if (this.y <= 0 || this.y + this.height >= 600) {
                    this.dodgeDirection *= -1;
                }

                this.dodgeTimer -= 100;

                if (this.dodgeTimer <= 0) {
                    this.isDodging = false;
                    this.dodgeTimer = 0;
                }
            } else {
                this.x -= this.speed;
            }

            if (this.x < enemyEndX) {
                endGame();
            }
        },

        // Metoda pro obdržení poškození
        takeDamage: function(damage) {
            this.health -= damage;

            this.speed *= 0.1;
            this.isDamaged = true;

            setTimeout(() => {
                this.speed = enemySpeed;
                this.isDamaged = false;
            }, 100);
        },
        draw: function(game) {
            let enemyElement = document.createElement('img');
            enemyElement.src = this.isDamaged ? this.damagedImage : this.image;
            enemyElement.style.position = 'absolute';
            enemyElement.style.left = this.x + 'px';
            enemyElement.style.top = this.y + 'px';
            enemyElement.style.width = this.width + 'px';
            enemyElement.style.height = this.height + 'px';
            game.appendChild(enemyElement);
        }
    },

    // Tovární funkce pro vytvoření nového nepřítele
    createEnemy: function(type) {
        let enemy = Object.create(this.EnemyPrototype);
        enemy.x = enemySpawnX;
        enemy.y = Math.random() * (enemySpawnYMax - enemySpawnYMin) + enemySpawnYMin;

        // Nastavíme vlastnosti nepřítele v závislosti na typu
        switch (type) {
            case 'default':
                enemy.width = 35;
                enemy.height = 35;
                enemy.image = 'images/Enemy.png';
                enemy.damagedImage = 'images/EnemyDamaged.png';
                break;
            case 'gg':
                enemy.width = 60; // Zvětšíme velikost
                enemy.height = 90;
                enemy.health = 6; // Zvýšíme zdraví
                enemy.speed = enemySpeed * 1.2; // Zvýšíme rychlost
                enemy.image = 'images/enemygg1.png';
                enemy.damagedImage = 'images/enemyggdamaged.png';
                break;
        }

        return enemy;
    }
};

// --- Konec prototypů a jmenných prostorů ---

// Vytvoříme loď hráče
let player = {
    x: 50,
    y: 300,
    dx: 0,
    dy: 0,
    ddx: 0,
    ddy: 0,
    friction: 0.98,
    maxSpeed: 1,
    width: 50,
    height: 40
};

// Vytvoříme pole pro nepřátele a střely
let enemies = [];
let shots = [];
let particles = [];

// Časovač pro objevování nepřátel
let enemySpawnTimer = null;

// Hranice pro objevování a pohyb nepřátel
const enemySpawnX = 1070;
const enemySpawnYMin = 20;
const enemySpawnYMax = 570 - 20;
const enemyEndX = 0;

// Funkce pro přehrávání zvukových efektů
function playSound(src, loop = false) {
    let sound = new Audio(src);
    sound.loop = loop;
    sound.volume = volume;
    sound.play();
    return sound;
}

// Funkce pro spuštění hry
function startGame() {
    console.log("Nová hra");
    gameActive = true;
    score = 0;
    enemyCount = 1;
    enemySpeed = 0.2;
    enemyHealth = 3;
    shotSpeed = 10;
    enemySpawnInterval = 7000;
    enemiesPerSpawn = 1;
    player.x = 50;
    player.y = 300;
    player.dx = 0;
    player.dy = 0;
    enemies = [];
    shots = [];
    particles = [];
    stopEnemySpawnTimer();
    firstEnemyLaunched = false;

    // Zkontrolujeme, zda se již přehrává Asgore
    if (gameMusic && gameMusic.src.includes('Asgore.mp3')) {
        gameMusic.currentTime = 0; // Přetočíme na začátek
    } else {
        // Zastavíme hudbu menu, pokud se přehrává
        if (menuMusic) {
            menuMusic.pause();
            menuMusic.volume = 0;
        }

        //  Zastavíme herní hudbu, pokud se liší od Asgore
        if (gameMusic) {
            menuMusic.pause();
            gameMusic.volume = 0;
        }

        // Vytvoříme a spustíme Asgore
        gameMusic = playSound('music/Asgore.mp3', true);
    }


    // Spustíme prvního nepřítele se zpožděním
    setTimeout(function() {
        firstEnemyLaunched = true;
        createEnemy();
    }, 2000); // 2 sekundy zpoždění

    startEnemySpawnTimer();

    document.getElementById('game').style.display = 'block';
    document.getElementById('earth').style.display = 'block';
    document.querySelector('.buttons').style.display = 'none';
    document.getElementById('logo').style.display = 'none';
    document.getElementById('score-container').style.display = 'flex';
}

function createEnemy() {
    // Zkontrolujeme příznak před vytvořením nepřítele
    if (!firstEnemyLaunched) {
        return;
    }

    let enemyType = 'default'; // Typ nepřítele ve výchozím nastavení

    // Podmínka pro objevení druhého typu nepřítele
    if (score >= 20 && Math.random() < 0.2) { // 20% šance na objevení po 10 bodech
        enemyType = 'gg';
    }

    // Použijeme tovární funkci z jmenného prostoru
    let enemy = EnemyNamespace.createEnemy(enemyType);
    enemies.push(enemy);
}


function applyRecoil(direction) {
    player.dx -= direction * 1;
}

// Funkce pro vytvoření nové střely
function createShot() {
    if (shots.length === 0) {
        let shot = {
            x: player.x + player.width,
            y: player.y + player.height / 2,
            width: 7,
            height: 2,
            dy: (Math.random() - 0.5) * 0.4
        };
        shots.push(shot);
        applyRecoil(1);
    }
}

// Obslužné rutiny událostí pro ovládání hry
window.addEventListener('keydown', function(event) {
    const acceleration = 0.05;
    switch (event.key) {
        case 'ArrowUp':
            player.ddy = -acceleration;
            break;
        case 'ArrowDown':
            player.ddy = acceleration;
            break;
        case 'ArrowLeft':
            player.ddx = -acceleration;
            break;
        case 'ArrowRight':
            player.ddx = acceleration;
            break;
        case ' ':
        case 'Enter':
            createShot();
            break;
        case 'R':
            if (!gameActive) {
                startGame();
            }
            break;
    }
});

window.addEventListener('keyup', function(event) {
    switch (event.key) {
        case 'ArrowUp':
        case 'ArrowDown':
            player.ddy = 0;
            break;
        case 'ArrowLeft':
        case 'ArrowRight':
            player.ddx = 0;
            break;
    }
});

// Funkce pro aktualizaci hry
function updateGame() {
    if (!gameActive) return;

    // Aktualizujeme rychlost a polohu lodi hráče
    player.dx += player.ddx;
    player.dy += player.ddy;
    player.dx *= player.friction;
    player.dy *= player.friction;
    player.x += player.dx;
    player.y += player.dy;


    if (player.dx > player.maxSpeed) player.dx = player.maxSpeed;
    if (player.dx < -player.maxSpeed) player.dx = -player.maxSpeed;
    if (player.dy > player.maxSpeed) player.dy = player.maxSpeed;
    if (player.dy < -player.maxSpeed) player.dy = -player.maxSpeed;


    player.x = Math.max(player.x, 0);
    player.x = Math.min(player.x, 1020 - player.width);
    player.y = Math.max(player.y, 0);
    player.y = Math.min(player.y, 600 - player.height);

    // Aktualizujeme polohu nepřátel
    enemies.forEach(function(enemy) {
        enemy.update(); // Použijeme metodu prototypu

        if (enemy.x < enemyEndX) {
            endGame();
        }
    });

    // Aktualizujeme polohu střel
    shots.forEach(function(shot, shotIndex) {
        shot.x += shotSpeed;
        shot.y += shot.dy;

        if (shot.x > game.offsetWidth) {
            shots.splice(shotIndex, 1);
        }
    });

    // Kontrolujeme kolize
    shots.forEach(function(shot, shotIndex) {
        enemies.forEach(function(enemy, enemyIndex) {

            if (shot.x + shotSpeed < enemy.x + enemy.width &&
                shot.x + shot.width + shotSpeed > enemy.x &&
                shot.y < enemy.y + enemy.height &&
                shot.y + shot.height > enemy.y) {

                enemy.takeDamage(1); // Použijeme metodu prototypu

                if (enemy.health <= 0) {
                    enemies.splice(enemyIndex, 1);
                    shots.splice(shotIndex, 1);

                    score++;

                    // Zvýšíme obtížnost při dosažení určitých bodů
                    increaseDifficulty();

                    createParticles(enemy);
                } else {
                    createParticles(enemy, Math.floor(Math.random() * 2) + 2);
                    shots.splice(shotIndex, 1);
                }
            }
        });
    });


    enemies = enemies.filter(function(enemy) {
        return enemy.x > 0;
    });

    // Aktualizujeme zobrazení skóre
    scoreElement.textContent = 'Score: ' + score;

    // Aktualizujeme částice
    particles.forEach(function(particle, index) {
        particle.x += particle.dx;
        particle.y += particle.dy;
        particle.opacity -= 0.01;
        particle.lifetime--;

        if (particle.lifetime <= 0) {
            particles.splice(index, 1);
        }
    });
}

// Funkce pro získání nejlepších výsledků z localStorage
function getHighScores() {
    let highScores = localStorage.getItem('highScores');
    return highScores ? JSON.parse(highScores) : { gamesPlayed: 0, maxScore: 0 };
}

// Funkce pro aktualizaci nejlepších výsledků v localStorage
function updateHighScores(newScore) {
    let highScores = getHighScores();
    highScores.gamesPlayed++;
    highScores.maxScore = Math.max(highScores.maxScore, newScore);
    localStorage.setItem('highScores', JSON.stringify(highScores));
}

function increaseDifficulty() {
    if (score === 2) {
        enemyCount = 5; // Nastavte požadovaný počet nepřátel pro tuto úroveň
        enemySpeed = 0.2; // Nastavte požadovanou rychlost pro tuto úroveň
        updateEnemySpawnInterval(6800); // Volitelné: můžete zde upravit interval objevování
        enemiesPerSpawn = 2;
    } else if (score === 15) {
        enemyCount = 10;
        enemySpeed = 0.2;
        updateEnemySpawnInterval(6500);
    } else if (score === 35) {
        enemyCount = 15;
        enemySpeed = 0.3;
        enemiesPerSpawn = 3;
        updateEnemySpawnInterval(6800);
    }
    else if (score === 50) {
        enemyCount = 15;
        enemySpeed = 0.3;
        updateEnemySpawnInterval(6600);
    }
    // ... přidejte další podmínky pro jiné prahové hodnoty skóre ...
}
// Funkce pro ukončení hry
function endGame() {
    gameActive = false;
    stopEnemySpawnTimer();

    // Zobrazíme obrazovku "Konec hry"
    document.getElementById('game-over-screen').style.display = 'block';
    document.getElementById('final-score').textContent = score;

    // Skryjeme ostatní prvky
    document.getElementById('game').style.display = 'none';
    document.getElementById('earth').style.display = 'none';
    document.getElementById('logo').style.display = 'block';
    document.getElementById('score-container').style.display = 'none';
    updateHighScores(score);
}
const scoresSection = document.getElementById('scores-section');
const gamesPlayedElement = document.getElementById('games-played');
const maxScoreElement = document.getElementById('max-score');
const backToMenuButton = document.getElementById('back-to-menu');

let scoresButton = document.createElement('button');
scoresButton.id = 'scoresButton';
scoresButton.textContent = 'Score';
document.querySelector('.buttons').appendChild(scoresButton);

// Funkce pro zobrazení nejlepších výsledků
function showScores() {
    let highScores = getHighScores();
    gamesPlayedElement.textContent = highScores.gamesPlayed;
    maxScoreElement.textContent = highScores.maxScore;

    // Skryjeme menu a zobrazíme sekci "Skóre"
    document.querySelector('.buttons').style.display = 'none';
    scoresSection.style.display = 'block';
    document.getElementById('logo').style.display = 'none';
    document.getElementById('score-container').style.display = 'none';
    let maxScoreComment = ""; // Inicializujeme řetězcem

    if (highScores.maxScore < 30) {
        maxScoreComment = " - you are pathetic";
    } else if (highScores.maxScore < 70) {
        maxScoreComment = " - not bad for a loser";
    } else {
        maxScoreComment = " - lol, didn't expect that";
    }

    // Přidáme komentář k textu elementu
    const maxScoreCommentElement = document.getElementById('max-score-comment');
    maxScoreCommentElement.textContent = maxScoreComment;
}
// Obslužná rutina tlačítka "Skóre"
scoresButton.addEventListener('click', showScores);

// Obslužná rutina tlačítka "Zpět do menu"
backToMenuButton.addEventListener('click', function() {
    // Skryjeme sekci "Skóre" a zobrazíme menu
    scoresSection.style.display = 'none';
    document.querySelector('.buttons').style.display = 'flex';
    document.getElementById('logo').style.display = 'block';
    playSound('music/reload.mp3');
});

// Obslužná rutina události pro tlačítko "Hrát znovu"
document.getElementById('restart-button').addEventListener('click', function() {
    document.getElementById('game-over-screen').style.display = 'none';
    startGame(); // Spustíme novou hru
    playSound('music/reload.mp3');
});

// Obslužná rutina události pro tlačítko "Hlavní menu"
document.getElementById('main-menu-button').addEventListener('click', function() {
    document.getElementById('game-over-screen').style.display = 'none';
    // Vrátíme hru do počátečního stavu
    document.getElementById('logo').style.display = 'block';
    document.querySelector('.buttons').style.display = 'flex'; // Zobrazíme tlačítka hlavního menu
    document.getElementById('game').style.display = 'none';
    document.getElementById('earth').style.display = 'none';
    document.getElementById('score-container').style.display = 'none';
    playSound('music/reload.mp3');

});

// Funkce pro vykreslení lodi hráče
function drawPlayer() {
    let game = document.getElementById('game');
    let playerElement = document.createElement('img');
    playerElement.src = 'images/Ship.png';
    playerElement.style.position = 'absolute';
    playerElement.style.left = player.x + 'px';
    playerElement.style.top = player.y + 'px';
    playerElement.style.width = player.width + 'px';
    playerElement.style.height = player.height + 'px';
    game.appendChild(playerElement);
}

// Funkce pro vykreslení hry
function drawGame() {
    let game = document.getElementById('game');


    while (game.firstChild) {
        game.removeChild(game.firstChild);
    }
    drawPlayer();
    enemies.forEach(function(enemy) {
        enemy.draw(game); // Použijeme metodu draw z prototypu
    });


    shots.forEach(function(shot) {
        let shotElement = document.createElement('div');
        shotElement.style.position = 'absolute';
        shotElement.style.left = shot.x + 'px';
        shotElement.style.top = shot.y + 'px';
        shotElement.style.width = shot.width + 'px';
        shotElement.style.height = shot.height + 'px';
        shotElement.style.backgroundColor = 'black';
        game.appendChild(shotElement);
    });

    // Vykreslíme částice
    particles.forEach(function(particle) {
        let particleElement = document.createElement('div');
        particleElement.style.position = 'absolute';
        particleElement.style.left = particle.x + 'px';
        particleElement.style.top = particle.y + 'px';
        particleElement.style.width = particle.size + 'px';
        particleElement.style.height = particle.size + 'px';
        particleElement.style.backgroundColor = 'black';
        particleElement.style.borderRadius = '50%';
        game.appendChild(particleElement);
    });
}

// Funkce pro vytvoření částic při zničení nepřítele
function createParticles(enemy, count = 10) {
    for (let i = 0; i < count; i++) {
        let particle = {
            x: enemy.x + enemy.width / 2,
            y: enemy.y + enemy.height / 2,
            size: 3 + (Math.random() - 1) * 1,
            dx: 3 + (Math.random() - 3) * 1,
            dy: 0 + (Math.random() - 1) * 1,
            gravity: Math.random() * 0.01 + 0.02,
            opacity: 1,
            lifetime: 150
        };
        particles.push(particle);
    }
}

// Aktualizujeme hru každých 20 milisekund
setInterval(function() {
    updateGame();
    drawGame();
    // Aktualizujeme částice
    particles.forEach(function(particle, index) {
        particle.x += particle.dx;
        particle.y += particle.dy;
        particle.dy += particle.gravity;
        particle.opacity -= 0.01;
        particle.lifetime--;
        // Odebereme částici, když její životnost skončí
        if (particle.lifetime <= 0) {
            particles.splice(index, 1);
        }
    });
}, 3);

// Získáme tlačítka
let startButton = document.getElementById('startButton');
// Obslužná rutina tlačítka "Start"
startButton.addEventListener('click', function() {
    playSound('music/reload.mp3');
    setTimeout(startGame, 500);
});

// Tlačítko ztlumení zvuku
let muteButton = document.createElement('button');
muteButton.id = 'muteButton';
muteButton.style.position = 'absolute';
muteButton.style.top = '10px';
muteButton.style.right = '10px';
muteButton.style.background = 'transparent';
muteButton.style.border = 'none';
document.body.appendChild(muteButton);

// Vytvoříme obrázek ve výchozím nastavení
let muteImage = document.createElement('img');
muteImage.src = 'images/Mute.png';
muteImage.style.width = '30px';
muteImage.style.height = '30px';
muteButton.appendChild(muteImage);

muteButton.addEventListener('click', function () {
    // Invertujeme hodnotu hlasitosti mezi 0 a 1
    volume = volume === 1 ? 0 : 1;

    if (volume === 0) {
        muteImage.src = 'images/Mute.png';
    } else {
        muteImage.src = 'images/Unmute.png';
    }

    // Použijeme hlasitost na všechny zvuky
    if (menuMusic) menuMusic.volume = volume;
    if (gameMusic) gameMusic.volume = volume;
    if (firstMuteClick && volume > 0 && (!gameMusic || !gameMusic.src.includes('Asgore.mp3'))) {
        menuMusic.play();
        firstMuteClick = false;
    }
});

// A ve funkci startEnemySpawnTimer voláme spawnEnemies místo createEnemy:
function startEnemySpawnTimer() {
    if (gameActive) {
        enemySpawnTimer = setTimeout(function() {
            spawnEnemies();
            // Rekurzivně spustíme časovač s novým náhodným intervalem
            startEnemySpawnTimer();
        }, Math.random() * (enemySpawnIntervalMax - enemySpawnIntervalMin) + enemySpawnIntervalMin);
    }
}


function stopEnemySpawnTimer() {
    clearInterval(enemySpawnTimer);
    enemySpawnTimer = null;
}

window.addEventListener('visibilitychange', function () {
    if (document.visibilityState === 'hidden') {
        stopEnemySpawnTimer();
    } else if (document.visibilityState === 'visible') {
        startEnemySpawnTimer();
    }
});


window.addEventListener('beforeunload', function () {
    stopEnemySpawnTimer();
});

function updateEnemySpawnInterval(newIntervalMin = null, newIntervalMax = null) {
    if (newIntervalMin !== null && newIntervalMax !== null) {
        enemySpawnIntervalMin = newIntervalMin;
        enemySpawnIntervalMax = newIntervalMax;
    }
    // Nastavíme interval objevování nepřátel s náhodným zpožděním
    stopEnemySpawnTimer();
    startEnemySpawnTimer();
}
// Změňte funkci volanou časovačem, aby se vytvořilo více nepřátel:
function spawnEnemies() {
    for (let i = 0; i < enemiesPerSpawn; i++) {
        // Vytvoříme nepřítele se zpožděním od 1 do 2 sekund
        setTimeout(createEnemy, Math.random() * 4000 + 2000);
    }
}
updateEnemySpawnInterval();